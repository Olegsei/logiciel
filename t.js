function printFunctionAnalyseCanonic(input) {
  document.getElementById("inputa").innerHTML = input.a;
  document.getElementById("inputh").innerHTML = input.h;
  document.getElementById("inputk").innerHTML = input.k;


    if (Number(input.k) != 0) {
        if (Number(input.a) > 0) {
            document.getElementById("AnalyseCanonicOutput").innerHTML = "<div>Image: [" + input.k + ", infini[</div>";
        } else {
            document.getElementById("AnalyseCanonicOutput").innerHTML = "<div>Image: ]infini, " + input.k + "[</div>";
            //printf("Image: ]infini, %f[\n", input.k);
        }
    } else {
        if (Number(input.a) > 0) {
          document.getElementById("AnalyseCanonicOutput").innerHTML = "<div>Image: [0, infini[</div>";
          //printf("Image: [0, infini[\n");
        } else {
          document.getElementById("AnalyseCanonicOutput").innerHTML = "<div>Image: ]infini, 0]</div>";
            //printf("Image: ]infini, 0]\n");
        }
    }
    //absisses a l'origine
    if (Number(input.k) != 0) {
      Number(input.ka);
        if (Number(input.ka) > 0) {
            document.getElementById("AnalyseCanonicOutput").innerHTML += "<div>x1 = " + input.x1 + ", x2 = " + input.x2 + "</div>";
            //printf("x1 = %f, x2 = %f\n", input.x1, input.x2);
        } else {
            document.getElementById("AnalyseCanonicOutput").innerHTML += "<div>Aucun 0</div>";
            //printf("Aucun 0\n");
        }
    } else {
        document.getElementById("AnalyseCanonicOutput").innerHTML += "<div>x = " + input.k + "</div>";
        //printf("x = %f\n", input.k);
    }
    //ordonnee a l'origine
    document.getElementById("AnalyseCanonicOutput").innerHTML += "<div>y = " + input.y + "</div>";
    //intf("y = %f\n", input.y);
    //domaine et image


    //Variation
    if (Number(input.a) > 0) {
        document.getElementById("AnalyseCanonicOutput").innerHTML += "<div>Fonction croissante sur [" + input.h +", infinte[</div>";
        document.getElementById("AnalyseCanonicOutput").innerHTML += "<div>Fonction decroissante sur ]-infinte, " + input.h +"] </div>";
        //intf("Fonction croissante sur [%f, infinte[\n", input.h);
        //intf("Fonction decroissante sur ]-infinte, %f] \n", input.h);
    } else if (Number(input.a) < 0) {
        document.getElementById("AnalyseCanonicOutput").innerHTML += "<div>Fonction decroissante sur ]-infinte, " + input.h +"] </div>";
        document.getElementById("AnalyseCanonicOutput").innerHTML += "<div>Fonction croissante sur [" + input.h +", infinte[</div>";
        //intf("Fonction croissante sur ]infinte, %f]\n", input.h);
        //intf("Fonction decroissante sur [%f, infinte[\n", input.h);
    } else {
        document.getElementById("AnalyseCanonicOutput").innerHTML += "<div>Aucune valeur de a</div>";
        //printf("Aucune valeur de a");
    }

    //signe
    if (Number(input.k) > 0 & Number(input.a) > 0) {
        document.getElementById("AnalyseCanonicOutput").innerHTML += "<div>f(x) > 0, pour tout x sur R</div>";
        //printf("f(x) > 0, pour tout x sur R\n");
    } else if (Number(input.k) == 0 & Number(input.a) > 0) {
        document.getElementById("AnalyseCanonicOutput").innerHTML += "<div>f(x) >= 0, pour tout x sur R</div>";
        //printf("f(x) >= 0, pour tout x sur R\n");
    } else if (Number(input.k) < 0 & Number(input.a) > 0) {
        document.getElementById("AnalyseCanonicOutput").innerHTML += "<div>f(x) >= 0, pour x faisant partie de ]infinite, "+input.x1+"] union ["+input.x2+", infinite[</div>";
        document.getElementById("AnalyseCanonicOutput").innerHTML += "<div>f(x) <= 0, pour x faisant partie de ["+input.x1+", "+input.x2+"]</div>";
        //printf("f(x) >= 0, pour x faisant partie de ]infinite, %f] union [%f, infinite[ \n", input.x1, input.x2);
        //printf("f(x) <= 0, pour x faisant partie de [%f, %f]\n", input.x1, input.x2);
    } else if (Number(input.k) == 0 & Number(input.a) < 0) {
        document.getElementById("AnalyseCanonicOutput").innerHTML += "<div>f(x) <= 0, pour tout x sur R</div>";
        //printf("f(x) <= 0, pour tout x sur R\n");
    } else if (Number(input.k) > 0 & Number(input.a) < 0) {
        document.getElementById("AnalyseCanonicOutput").innerHTML += "<div>f(x) <= 0, pour x faisant partie de ]infinite, "+input.x1+"] union ["+input.x2+", infinite[</div>";
        document.getElementById("AnalyseCanonicOutput").innerHTML += "<div>f(x) >= 0, pour x faisant partie de ["+input.x1+", "+input.x2+"]</div>";
        //printf("f(x) <= 0, pour x faisant partie de ]infinite, %f] union [%f, infinite[ \n", input.x1, input.x2);
        //printf("f(x) >= 0, pour x faisant partie de [%f, %f]\n", input.x1, input.x2);
    } else if (Number(input.k) < 0 & Number(input.a) < 0) {
        document.getElementById("AnalyseCanonicOutput").innerHTML += "<div>f(x) < 0, pour tout x sur R</div>";
        //printf("f(x) < 0, pour tout x sur R\n");
    } else {
        document.getElementById("AnalyseCanonicOutput").innerHTML += "<div>Aucune donnee de a</div>";
        //printf("Aucune donnee de a\n");
    }
    //Extremums
    if (Number(input.a) > 0) {
        document.getElementById("AnalyseCanonicOutput").innerHTML += "<div>minimum = "+input.k+"</div>";
        //printf("minimum = %f\n", input.k);
    } else if (Number(input.a) < 0) {
        document.getElementById("AnalyseCanonicOutput").innerHTML += "<div>maximum = "+input.k+"</div>";
        //printf("maximum = %f\n", input.k);
    } else {
        document.getElementById("AnalyseCanonicOutput").innerHTML += "<div>Aucune donnee de a</div>";
        //printf("Aucune donee a\n");
    }

}
function mainQanalyseCanonique() {
  event.preventDefault();

  var res = {
          a: document.getElementById('formQanalyseCanoniquea').value,
          h: document.getElementById('formQanalyseCanoniqueh').value,
          k: document.getElementById('formQanalyseCanoniquek').value
  };

  if (Number(res.k) != 0) {
      res.ka = -Number(res.k) / Number(res.a);
      if (res.ka > 0) {
          var srd = Math.sqrt(Number(res.ka));
          res.x1 = Number(res.h) - Number(srd);
          res.x2 = Number(srd) + Number(res.h);
      }
  }

  res.y = (Number(res.a) * Number(res.h) * Number(res.h)) + Number(res.k);

  printFunctionAnalyseCanonic(res);
}

function printFunctionAnalyseGenerale(input) {
  document.getElementById("inputA").innerHTML = input.A;
  document.getElementById("inputB").innerHTML = input.B;
  document.getElementById("inputC").innerHTML = input.C;

  console.log(input);

    //printf("f(x) = %f^2 + %fx + %f\n", input.A, input.B, input.C);
    document.getElementById("AnalyseGeneraleOutput").innerHTML = "<div>f(x) = "+input.A+"x^2 + "+input.B+"x + "+input.C+"</div>";

   if (Number(input.k) != 0) {
      Number(input.ka);
        if (Number(input.ka) > 0) {
            document.getElementById("AnalyseGeneraleOutput").innerHTML += "<div>x1 = "+input.x1+", x2 = "+input.x2+"</div>";
            //printf("x1 = %f, x2 = %f\n", input.x1, input.x2);
        } else {
            document.getElementById("AnalyseGeneraleOutput").innerHTML += "<div>Aucun 0</div>";
            //printf("Aucun 0\n");
        }
    } else {
        document.getElementById("AnalyseGeneraleOutput").innerHTML += "<div>x = "+input.k+"</div>";
        //printf("x = %f\n", input.k);
    }
    //ordonnee a l'origine
    document.getElementById("AnalyseGeneraleOutput").innerHTML += "<div>y = "+input.y+"</div>";
    //printf("y = %f\n", input.y);
    //domaine et image

    //printf("Domaine: R\n");

    if (Number(input.k) > 0 & Number(input.A) > 0) {
        document.getElementById("AnalyseGeneraleOutput").innerHTML += "<div>Image: ["+input.k+", infini[</div>";
        //printf("Image: [%f, infini[\n", input.k);
    } else if (Number(input.k) == 0 & Number(input.A) > 0) {
        document.getElementById("AnalyseGeneraleOutput").innerHTML += "<div>Image: [0, infini[</div>";
        //printf("Image: [0, infini[\n");
    } else if (Number(input.k) < 0 & Number(input.A) > 0) {
        document.getElementById("AnalyseGeneraleOutput").innerHTML += "<div>Image: ["+input.k+", infini[</div>";
        //printf("Image: [%f, infini[\n", input.k);
    } else if (Number(input.k) == 0 & Number(input.A) < 0) {
        document.getElementById("AnalyseGeneraleOutput").innerHTML += "<div>Image: ]infini, 0]</div>";
        //printf("Image: ]infini, 0]\n");
    } else if (Number(input.k) > 0 & Number(input.A) < 0) {
        document.getElementById("AnalyseGeneraleOutput").innerHTML += "<div>Image: ]infini, "+input.k+"[</div>";
        //printf("Image: ]infini, %f[\n", input.k);
    } else if (Number(input.k) < 0 & Number(input.A) < 0) {
        document.getElementById("AnalyseGeneraleOutput").innerHTML += "<div>Image: ]infini, "+input.k+"]</div>";
        //printf("Image: ]infini, %f]\n", input.k);
    } else {
        document.getElementById("AnalyseGeneraleOutput").innerHTML += "<div>Aucune donnee de a</div>";
        //printf("Aucune donnee de a\n");
    }

    //Variation
    if (Number(input.A) > 0) {
        document.getElementById("AnalyseGeneraleOutput").innerHTML += "<div>Fonction croissante sur ["+input.h+", infinte[</div>";
        document.getElementById("AnalyseGeneraleOutput").innerHTML += "<div>Fonction decroissante sur ]-infinte, "+input.h+"]</div>";
        //printf("Fonction croissante sur [%f, infinte[\n", input.h);
        //printf("Fonction decroissante sur ]-infinte, %f] \n", input.h);
    } else if (Number(input.A) < 0) {
        document.getElementById("AnalyseGeneraleOutput").innerHTML += "<div>Fonction croissante sur ]infinte, "+input.h+"]</div>";
        document.getElementById("AnalyseGeneraleOutput").innerHTML += "<div>Fonction decroissante sur ["+input.h+", infinte[</div>";
        //printf("Fonction croissante sur ]infinte, %f]\n", input.h);
        //printf("Fonction decroissante sur [%f, infinte[\n", input.h);
    } else {
        document.getElementById("AnalyseGeneraleOutput").innerHTML += "<div>Aucune valeur de a</div>";
        //printf("Aucune valeur de a");
    }

    //signe
    if (Number(input.k) > 0 & Number(input.A) > 0) {
        document.getElementById("AnalyseGeneraleOutput").innerHTML += "<div>f(x) > 0, pour tout x sur R</div>";
        //printf("f(x) > 0, pour tout x sur R\n");
    } else if (Number(input.k) == 0 & Number(input.A) > 0) {
        document.getElementById("AnalyseGeneraleOutput").innerHTML += "<div>f(x) >= 0, pour tout x sur R</div>";
        //printf("f(x) >= 0, pour tout x sur R\n");
    } else if (Number(input.k) < 0 & Number(input.A) > 0) {
        document.getElementById("AnalyseGeneraleOutput").innerHTML += "<div>f(x) >= 0, pour x faisant partie de ]infinite, "+input.x1+"] union ["+input.x2+", infinite[</div>";
        document.getElementById("AnalyseGeneraleOutput").innerHTML += "<div>f(x) <= 0, pour x faisant partie de ["+input.x1+", "+input.x2+"]</div>";
        //printf("f(x) >= 0, pour x faisant partie de ]infinite, %f] union [%f, infinite[ \n", input.x1, input.x2);
        //printf("f(x) <= 0, pour x faisant partie de [%f, %f]\n", input.x1, input.x2);
    } else if (Number(input.k) == 0 & Number(input.A) < 0) {
        document.getElementById("AnalyseGeneraleOutput").innerHTML += "<div>f(x) <= 0, pour tout x sur R</div>";
        //printf("f(x) <= 0, pour tout x sur R\n");
    } else if (Number(input.k) > 0 & Number(input.A) < 0) {
        document.getElementById("AnalyseGeneraleOutput").innerHTML += "<div>f(x) <= 0, pour x faisant partie de ]infinite, "+input.x1+"] union ["+input.x2+", infinite[</div>";
        document.getElementById("AnalyseGeneraleOutput").innerHTML += "<div>f(x) >= 0, pour x faisant partie de ["+input.x1+", "+input.x2+"]</div>";
        //printf("f(x) <= 0, pour x faisant partie de ]infinite, %f] union [%f, infinite[ \n", input.x1, input.x2);
        //printf("f(x) >= 0, pour x faisant partie de [%f, %f]\n", input.x1, input.x2);
    } else if (Number(input.k) < 0 & Number(input.A) < 0) {
        document.getElementById("AnalyseGeneraleOutput").innerHTML += "<div>f(x) < 0, pour tout x sur R</div>";
        //printf("f(x) < 0, pour tout x sur R\n");
    } else {
        document.getElementById("AnalyseGeneraleOutput").innerHTML += "<div>Aucune donnee de a</div>";
        //printf("Aucune donnee de a\n");
    }

    //Extremums
    if (Number(input.A) > 0) {
        document.getElementById("AnalyseGeneraleOutput").innerHTML += "<div>minimum = "+input.k+"</div>";
        //printf("minimum = %f\n", input.k);
    } else if (Number(input.A) < 0) {
        document.getElementById("AnalyseGeneraleOutput").innerHTML += "<div>maximum = "+input.k+"</div>";
        //printf("maximum = %f\n", input.k);
    } else {
        document.getElementById("AnalyseGeneraleOutput").innerHTML += "<div>Aucune donee a</div>";
        //printf("Aucune donee a\n");
    }
}
function mainQanalyseGenerale() {
  event.preventDefault();

  var res = {
            A: document.getElementById('formQanalyseGeneraleA').value,
            B: document.getElementById('formQanalyseGeneraleB').value,
            C: document.getElementById('formQanalyseGeneraleC').value
    };
console.log(document.getElementById('formQanalyseGeneraleA').value);
    res.h = (-1 * (Number(res.B))) / (2 * Number(res.A));
    res.k = Number(res.A) * Number(res.h) * Number(res.h) + Number(res.B) * Number(res.h) + Number(res.C);
    if (Number(res.k) != 0) {
        res.ka = -Number(res.k) / Number(res.A);
        if (Number(res.ka) > 0) {
            var srd = Math.sqrt(Number(res.ka));
            res.x1 = Number(res.h) - Number(srd);
            res.x2 = Number(srd) + Number(res.h);
        }
    }
    res.y = (Number(res.A) * Number(res.h) * Number(res.h)) + Number(res.k);

    printFunctionAnalyseGenerale(res);
}

function printFunctionAnalyseFactorisee(input) {
  document.getElementById("inputaf").innerHTML = input.a;
  document.getElementById("inputx1f").innerHTML = input.x1;
  document.getElementById("inputx2f").innerHTML = input.x2;

  //document.getElementById("AnalyseFactoriseeOutput").innerHTML = "<div>f(x) = "+input.a+"(x-"+input.x1+")(x-"+input.x2+")</div>";

 //printf("f(x) = %f(x-%f)(x-%f)\n", input.a, input.x1, input.x2);
    //coordonnees a l'origine
    //absisses a l'origine
    document.getElementById("AnalyseFactoriseeOutput").innerHTML = "<div>x1 = "+input.x1+", x2 = "+input.x2+"</div>";
    //printf("x1 = %f, x2 = %f\n", input.x1, input.x2);

    document.getElementById("AnalyseFactoriseeOutput").innerHTML += "<div>y = "+input.y+" </div>";
    //printf("y = %f\n", input.y);

    //domaine et image
    document.getElementById("AnalyseFactoriseeOutput").innerHTML += "<div>Domaine: R </div>";
    //printf("Domaine: R\n");

    if (Number(input.k) > 0 & Number(input.a) > 0) {
        document.getElementById("AnalyseFactoriseeOutput").innerHTML += "<div>Image: ["+input.k+", infini[ </div>";
        //printf("Image: [%f, infini[\n", input.k);
    } else if (Number(input.k) == 0 & Number(input.a) > 0) {
        document.getElementById("AnalyseFactoriseeOutput").innerHTML += "<div>Image: [0, infini[</div>";
        //printf("Image: [0, infini[\n");
    } else if (Number(input.k) < 0 & Number(input.a) > 0) {
        document.getElementById("AnalyseFactoriseeOutput").innerHTML += "<div>Image: ["+input.k+", infini[ </div>";
        //printf("Image: [%f, infini[\n", input.k);
    } else if (Number(input.k) == 0 & Number(input.a) < 0) {
        document.getElementById("AnalyseFactoriseeOutput").innerHTML += "<div>Image: ]infini, 0] </div>";
        //printf("Image: ]infini, 0]\n");
    } else if (Number(input.k) > 0 & Number(input.a) < 0) {
        document.getElementById("AnalyseFactoriseeOutput").innerHTML += "<div>Image: ]infini, "+input.k+"[ </div>";
        //printf("Image: ]infini, %f[\n", input.k);
    } else if (Number(input.k) < 0 & Number(input.a) < 0) {
        document.getElementById("AnalyseFactoriseeOutput").innerHTML += "<div>Image: ]infini, "+input.k+"] </div>";
        //printf("Image: ]infini, %f]\n", input.k);
    } else {
        document.getElementById("AnalyseFactoriseeOutput").innerHTML += "<div>Aucune donnee de a </div>";
        //printf("Aucune donnee de a\n");
    }

    //Variation
    if (Number(input.a) > 0) {
        document.getElementById("AnalyseFactoriseeOutput").innerHTML += "<div>Fonction croissante sur ["+input.h+", infinte[ </div>";
        document.getElementById("AnalyseFactoriseeOutput").innerHTML += "<div>Fonction decroissante sur ]-infinte, "+input.h+"] </div>";
        //printf("Fonction croissante sur [%f, infinte[\n", input.h);
        //printf("Fonction decroissante sur ]-infinte, %f] \n", input.h);
    } else if (Number(input.a) < 0) {
        document.getElementById("AnalyseFactoriseeOutput").innerHTML += "<div>Fonction croissante sur ]infinte, "+input.h+"] </div>";
        document.getElementById("AnalyseFactoriseeOutput").innerHTML += "<div>Fonction decroissante sur ["+input.h+", infinte[ </div>";
        //printf("Fonction croissante sur ]infinte, %f]\n", input.h);
        //printf("Fonction decroissante sur [%f, infinte[\n", input.h);
    } else {
        document.getElementById("AnalyseFactoriseeOutput").innerHTML += "<div>Aucune valeur de a </div>";
        //printf("Aucune valeur de a");
    }

    //signe
    if (Number(input.k) > 0 & Number(input.a) > 0) {
        document.getElementById("AnalyseFactoriseeOutput").innerHTML += "<div>f(x) > 0, pour tout x sur R </div>";
        //printf("f(x) > 0, pour tout x sur R\n");
    } else if (Number(input.k) == 0 & Number(input.a) > 0) {
        document.getElementById("AnalyseFactoriseeOutput").innerHTML += "<div>f(x) >= 0, pour tout x sur R </div>";
        //printf("f(x) >= 0, pour tout x sur R\n");
    } else if (Number(input.k) < 0 & Number(input.a) > 0) {
        document.getElementById("AnalyseFactoriseeOutput").innerHTML += "<div>f(x) >= 0, pour x faisant partie de ]infinite, "+input.x1+"] union ["+input.x2+", infinite[ </div>";
        document.getElementById("AnalyseFactoriseeOutput").innerHTML += "<div>f(x) <= 0, pour x faisant partie de ["+input.x1+", "+input.x2+"] </div>";
        //printf("f(x) >= 0, pour x faisant partie de ]infinite, %f] union [%f, infinite[ \n", input.x1, input.x2);
        //printf("f(x) <= 0, pour x faisant partie de [%f, %f]\n", input.x1, input.x2);
    } else if (Number(input.k) == 0 & Number(input.a) < 0) {
        document.getElementById("AnalyseFactoriseeOutput").innerHTML += "<div>f(x) <= 0, pour tout x sur R </div>";
        //printf("f(x) <= 0, pour tout x sur R\n");
    } else if (Number(input.k) > 0 & Number(input.a) < 0) {
        document.getElementById("AnalyseFactoriseeOutput").innerHTML += "<div>f(x) <= 0, pour x faisant partie de ]infinite, "+input.x1+"] union ["+input.x2+", infinite[ </div>";
        document.getElementById("AnalyseFactoriseeOutput").innerHTML += "<div>f(x) >= 0, pour x faisant partie de ["+input.x1+", "+input.x2+"] </div>";
        //printf("f(x) <= 0, pour x faisant partie de ]infinite, %f] union [%f, infinite[ \n", input.x1, input.x2);
        //printf("f(x) >= 0, pour x faisant partie de [%f, %f]\n", input.x1, input.x2);
    } else if (Number(input.k) < 0 & Number(input.a) < 0) {
        document.getElementById("AnalyseFactoriseeOutput").innerHTML += "<div>f(x) < 0, pour tout x sur R </div>";
        //printf("f(x) < 0, pour tout x sur R\n");
    } else {
        document.getElementById("AnalyseFactoriseeOutput").innerHTML += "<div>Aucune donnee de a </div>";
        //printf("Aucune donnee de a\n");
    }

    //Extremums
    if (Number(input.a) > 0) {
        document.getElementById("AnalyseFactoriseeOutput").innerHTML += "<div>minimum = "+input.k+" </div>";
        //printf("minimum = %f\n", input.k);
    } else if (Number(input.a) < 0) {
        document.getElementById("AnalyseFactoriseeOutput").innerHTML += "<div>maximum = "+input.k+" </div>";
        //printf("maximum = %f\n", input.k);
    } else {
        document.getElementById("AnalyseFactoriseeOutput").innerHTML += "<div>Aucune donee a </div>";
        //printf("Aucune donee a\n");
    }
}
function mainQanalyseFactorisee() {
  event.preventDefault();

  var res = {
            a: document.getElementById('formQanalyseFactoriseea').value,
            x1: document.getElementById('formQanalyseFactoriseex1').value,
            x2: document.getElementById('formQanalyseFactoriseex2').value
  };
  res.h = (Number(res.x1) + Number(res.x2)) / 2;
  res.k = Number(res.a) * (Number(res.h) - Number(res.x1)) * (Number(res.h) - Number(res.x2));

  res.y = (Number(res.a) * Number(res.h) * Number(res.h)) + Number(res.k);

     if (Number(res.k) < 0 & Number(res.a) > 0) {
        var ka = -Number(res.k) / Number(res.a);
        var srd = Math.sqrt(Number(res.ka));
        res.x1 = Number(res.h) - Number(srd);
        res.x2 = Number(srd) + Number(res.h);
    } else if (Number(res.k) > 0 & Number(res.a) < 0) {
        var ka = -Number(res.k) / Number(res.a);
        var srd = Math.sqrt(Number(res.ka));
        res.x1 = Number(res.h) - Number(srd);
        res.x2 = Number(srd) + Number(res.h);
    }

    printFunctionAnalyseFactorisee(res);
}

function printFunctionCanonicCaseY(input) {
  document.getElementById("CanonicCaseYOutput").innerHTML = "<div>y = "+input.y+"</div>";
  document.getElementById("CanonicCaseYOutput").innerHTML += "<div>f(x) = "+input.a+"("+input.x+"-"+input.h+")^2 + "+input.k+"</div>";
  //printf("y = %f \n", input.y);
  //printf("f(x) = %f(%f-%f)^2 + %f\n", input.a, input.x, input.h, input.k);
}
function mainCanonicCaseY(){
  var res = {
        x: document.getElementById('formCanonicCaseYx').value,
        a: document.getElementById('formCanonicCaseYa').value,
        h: document.getElementById('formCanonicCaseYh').value,
        k: document.getElementById('formCanonicCaseYk').value
    };
    res.y = Number(res.a) * ((Number(res.x) - Number(res.h)) * (Number(res.x) - Number(res.h))) + Number(res.k);

    printFunctionCanonicCaseY(res);
}

function printFunctionCanonicCaseX(input) {
  if(Number(input.k) > Number(input.y)){
  document.getElementById("CanonicCaseXOutput").innerHTML = "<div> Pas de solution</div>";
  }
  else if(Number(input.k) == Number(input.y)){
  document.getElementById("CanonicCaseXOutput").innerHTML = "<div>x1,2 = "+input.x1+"</div>";
  }
  else{
  document.getElementById("CanonicCaseXOutput").innerHTML = "<div>x1 = "+input.x1+", x2 = "+input.x2+"</div>";
  document.getElementById("CanonicCaseXOutput").innerHTML += "<div>"+input.y+" = "+input.a+"(x-"+input.h+")^2 + "+input.k+"</div>";
  }//printf("x1 = %f, x2 = %f\n", input.x1, input.x2);
  //printf("%f = %f(x-%f)^2 + %f\n", input.y, input.a, input.h, input.k);

}
function mainCanonicCaseX(){
  var res = {
        y: document.getElementById('formCanonicCaseXy').value,
        a: document.getElementById('formCanonicCaseXa').value,
        h: document.getElementById('formCanonicCaseXh').value,
        k: document.getElementById('formCanonicCaseXk').value
    };
    var t = Math.sqrt((Number(res.y) - Number(res.k)) / Number(res.a));
    res.x1 = Number(res.h) - Number(t);
    res.x2 = Number(t) + Number(res.h);

    printFunctionCanonicCaseX(res);
  }

function printFunctionCanonicCaseA(input) {
  document.getElementById("CanonicCaseAOutput").innerHTML = "<div>a = "+input.a+"</div>";
  document.getElementById("CanonicCaseAOutput").innerHTML += "<div>"+input.y+" = a("+input.x+"-"+input.h+")^2 + "+input.k+"</div>";
  //printf("a = %f \n", input.a);
  //printf("%f = a(%f-%f)^2 + %f\n", input.y, input.x, input.h, input.k);
}
function mainCanonicCaseA(){
  var res = {
        y: document.getElementById('formCanonicCaseAy').value,
        x: document.getElementById('formCanonicCaseAx').value,
        h: document.getElementById('formCanonicCaseAh').value,
        k: document.getElementById('formCanonicCaseAk').value
    };
    res.a = (Number(res.y) - Number(res.k)) / ((Number(res.x) - Number(res.h)) * (Number(res.x) - Number(res.h)));

    printFunctionCanonicCaseA(res);
  }

function printFunctionCanonicCaseH(input) {
  if(Number(input.k) > Number(input.y)){
  document.getElementById("CanonicCaseHOutput").innerHTML = "<div>Pas de solution</div>";
  }
  else if(Number(input.k) == Number(input.y)){
  document.getElementById("CanonicCaseHOutput").innerHTML = "<div>h = "+input.h+"</div>";
  document.getElementById("CanonicCaseHOutput").innerHTML += "<div>"+input.y+" = "+input.a+"("+input.x+"-h)^2 + "+input.k+"</div>";
}
else{
  document.getElementById("CanonicCaseHOutput").innerHTML = "<div>h1 = "+input.h1+", h2 = "+input.h2+"</div>";
  document.getElementById("CanonicCaseHOutput").innerHTML += "<div>"+input.y+" = "+input.a+"("+input.x+"-h)^2 + "+input.k+"</div>";
}
  //printf("%f = %f(%f-h)^2 + %f\n", input.y, input.x, input.a, input.k);
}
function mainCanonicCaseH(){
  var res = {
        y: document.getElementById('formCanonicCaseHy').value,
        x: document.getElementById('formCanonicCaseHx').value,
        a: document.getElementById('formCanonicCaseHa').value,
        k: document.getElementById('formCanonicCaseHk').value
    };
    var t = Math.sqrt((Number(res.y) - Number(res.k)) / Number(res.a));
    res.h1 = -1 * (Number(t) - Number(res.x));
    res.h2 = (Number(res.x) + Number(t));

    printFunctionCanonicCaseH(res);
  }

function printFunctionCanonicCaseK(input) {
  document.getElementById("CanonicCaseKOutput").innerHTML = "<div>k = "+input.k+" </div>";
  document.getElementById("CanonicCaseKOutput").innerHTML += "<div>"+input.y+" = "+input.a+"("+input.x+"-"+input.h+")^2 + k</div>";
  //printf("k = %f \n", input.k);
  //printf("%f = %f(%f-%f)^2 + k\n", input.y, input.x, input.a, input.h);
}
function mainCanonicCaseK(){
  var res = {
        y: document.getElementById('formCanonicCaseKy').value,
        x: document.getElementById('formCanonicCaseKx').value,
        a: document.getElementById('formCanonicCaseKa').value,
        h: document.getElementById('formCanonicCaseKh').value
    };
    res.k = Number(res.y) - (Number(res.a) * ((Number(res.x) - Number(res.h)) * (Number(res.x) - Number(res.h))));

    printFunctionCanonicCaseK(res);
  }

function printFunctionTransformCanonic(input) {

      Number(input.ka);
      if (Number(input.ka) > 0) {
          document.getElementById("TransformCanonicOutput").innerHTML = "<div>Forme factorisee: f(x) = "+input.a+"(x-"+input.x1+")(x-"+input.x2+")</div>";
          //printf("Forme factorisee: f(x) = %f(x-%f)(x-%f)\n", input.a, input.x1, input.x2);
      } else {
          document.getElementById("TransformCanonicOutput").innerHTML += "<div>Aucun 0</div>";
          //printf("Aucun 0\n");
      }
      document.getElementById("TransformCanonicOutput").innerHTML += "<div>Generale: f(x) = "+input.a+"x^2 + "+input.B+"x + "+input.C+"</div>";
      //printf("Generale: f(x) = %fx^2 + %fx + %f\n", input.A, input.B, input.C);
    }
function mainTransformCanonic(){
  var res = {
        a: document.getElementById('formTransformCanonica').value,
        h: document.getElementById('formTransformCanonich').value,
        k: document.getElementById('formTransformCanonick').value
    };
    res.ka = -1 * (Number(res.k)) / Number(res.a);
      if (Number(res.ka) > 0) {
          var srd = Math.sqrt(Number(res.ka));
          res.x1 = Number(res.h) - Number(srd);
          res.x2 = Number(res.h) + Number(srd);
    }
    res.B = Number(res.a) * -(Number(res.h) + Number(res.h));
    res.C = Number(res.a) * Number(res.h) * Number(res.h) + Number(res.k);

    printFunctionTransformCanonic(res);
  }

function printFunctionTransformGenerale(input) {

    Number(input.delta);
  if (Number(input.delta) > 0) {
      document.getElementById("TransformGeneraleOutput").innerHTML = "<div>Forme factorisee: f(x) = "+input.A+"(x-"+input.x1+")(x-"+input.x2+")</div>";
      //printf("Forme factorisee: f(x) = %f(x-%f)(x-%f)\n", input.A, input.x1, input.x2);
  } else {
      document.getElementById("TransformGeneraleOutput").innerHTML += "<div>Aucun 0</div>";
      //printf("Aucun 0\n");
  }
  document.getElementById("TransformGeneraleOutput").innerHTML += "<div>Forme canonique: f(x) = "+input.A+"(x-"+input.h+")^2 + "+input.k+"</div>";
  //printf("Forme canonique: f(x) = %f(x-%f)^2 + %f\n", input.A, input.h, input.k);
}
function mainTransformGenerale(){
  var res = {
        A: document.getElementById('formTransformGeneraleA').value,
        B: document.getElementById('formTransformGeneraleB').value,
        C: document.getElementById('formTransformGeneraleC').value
    };
    res.h = (-1 * (Number(res.B))) / (2 * Number(res.A));
    var i = Number(res.A) * Number(res.h) * Number(res.h);
    var o = Number(res.B) * Number(res.h) + Number(res.C);
    res.k = Number(i) + Number(o);
    res.delta = ((Number(res.B) * Number(res.B)) - (4 * Number(res.A) * Number(res.C)));
    if (Number(res.delta) > 0) {
        var srd = Math.sqrt(Number(res.delta));
        res.x1 = (-Number(res.B) - Number(srd)) / (2 * Number(res.A));
        res.x2 = (-Number(res.B) + Number(srd)) / (2 * Number(res.A));
    }

    printFunctionTransformGenerale(res);
  }

function printFunctionTransformFactorisee(input) {

      document.getElementById("TransformFactoriseeOutput").innerHTML = "<div>x1 = "+input.x1+", x2 = "+input.x2+"</div>";
      document.getElementById("TransformFactoriseeOutput").innerHTML += "<div>Forme canonique: f(x) = "+input.a+"(x-"+input.h+")^2 + "+input.k+"</div>";
      document.getElementById("TransformFactoriseeOutput").innerHTML += "<div>Generale: f(x) = "+input.a+"x^2 + "+input.B+"x + "+input.C+"</div>";
      //printf("x1 = %f, x2 = %f\n", input.x1, input.x2);
      //printf("Forme canonique: f(x) = %f(x-%f)^2 + %f\n", input.a, input.h, input.k);
      //printf("Generale: f(x) = %fx^2 + %fx + %f\n", input.a, input.B, input.C);
    }
function mainTransformFactorisee(){
  var res = {
        a: document.getElementById('formTransformFactoriseea').value,
        x1: document.getElementById('formTransformFactoriseex1').value,
        x2: document.getElementById('formTransformFactoriseex2').value
    };
    res.B = Number(res.a) * (-Number(res.x2) + -Number(res.x1));
    res.C = Number(res.a) * (Number(res.x1) * Number(res.x2));
    res.h = (Number(res.x1) / 2) + (Number(res.x2) / 2);
    res.k = Number(res.a) * (Number(res.h) - Number(res.x1)) * (Number(res.h) - Number(res.x2));

    printFunctionTransformFactorisee(res);
}

function printFunctionAnalysePE(input) {
  document.getElementById("inputap").innerHTML = input.a;
  document.getElementById("inputbp").innerHTML = input.b;
  document.getElementById("inputhp").innerHTML = input.h;
  document.getElementById("inputkp").innerHTML = input.k;

   Number(input.ab);
      if(Number(input.ab) > 0){
        document.getElementById("AnalysePEOutput").innerHTML = "<div>Variation: La foncton est croissante </div>";
        //printf("Variation: La foncton est croissante\n");
       }
      else{
        document.getElementById("AnalysePEOutput").innerHTML += "<div>Variation: La foncton est décroissante</div>";
        //printf("Variation: La fonction est décroissante\n;");
       }
        document.getElementById("AnalysePEOutput").innerHTML += "<div>f(x) = "+input.a+" ["+input.b+" (x-"+input.h+") ] + "+input.k+"</div>";
        document.getElementById("AnalysePEOutput").innerHTML += "<div>Domaine: R, Image: Z</div>";
        document.getElementById("AnalysePEOutput").innerHTML += "<div>Extremums: Il n'y en a pas</div>";
      //printf("f(x) = %f [%f (x-%f) ] + %f\n", input.a, input.b, input.h, input.k);
      //printf("Domaine: R, Image: Z\n");
//printf("Extremums: Il n'y en a pas\n");
}
function mainAnalysePE(){
  var res = {
        a: document.getElementById('formAnalysePEa').value,
        b: document.getElementById('formAnalysePEb').value,
        h: document.getElementById('formAnalysePEh').value,
        k: document.getElementById('formAnalysePEk').value
    };
    res.ab = Number(res.a) * Number(res.b);

    printFunctionAnalysePE(res);
}

function printFunctionPEcaseY(input) {

     Number(input.fpe);
     if(Number(input.fpe) > 0){
       document.getElementById("PEcaseYOutput").innerHTML = "<div>y = "+input.y+"</div>";
      //printf("y = %f \n", input.y);
      }
      else if(Number(input.fpe) < 0){
        document.getElementById("PEcaseYOutput").innerHTML += "<div>y = "+input.y+"</div>";
      //printf("y = %f \n", input.y);
      }
      else{
        document.getElementById("PEcaseYOutput").innerHTML += "<div>y = "+input.k+"</div>";
      //printf("y = %f \n", input.k);
      }
        document.getElementById("PEcaseYOutput").innerHTML += "<div>f(x) = "+input.a+" ["+input.b+" ("+input.x+"-"+input.h+")] + "+input.k+"</div>";
      //printf("f(x) = %f [%f (%f-%f) ] + %f\n", input.a, input.b, input.x, input.h, input.k);
}
function mainPEcaseY(){
  var res = {
        x: document.getElementById('formPEcaseYx').value,
        a: document.getElementById('formPEcaseYa').value,
        b: document.getElementById('formPEcaseYb').value,
        h: document.getElementById('formPEcaseYh').value,
        k: document.getElementById('formPEcaseYk').value
    };
    res.fpe = (Number(res.x) - Number(res.h)) * Number(res.b);
      if(Number(res.fpe) > 0)
      {
       var intpart = Math.trunc(Number(res.fpe));
       var m = (Number(res.a) * Number(intpart));
       res.y = Number(m) + Number(res.k);
      }
      else if(Number(res.fpe) < 0)
      {
       var intpart = (Math.trunc(Number(res.fpe))) - 1;
       var n = (Number(res.a) * Number(intpart));
       res.y = Number(n) + Number(res.k);
      }

    printFunctionPEcaseY(res);
}

function printFunctionPEcaseA(input) {

      Number(input.fpe);
       if(Number(input.fpe) > 0){
         document.getElementById("PEcaseAOutput").innerHTML = "<div>a = "+input.a+"</div>";
        //printf("a = %f \n", input.a);
       }
       else if(Number(input.fpe) < 0){
         document.getElementById("PEcaseAOutput").innerHTML += "<div>a = "+input.a+"</div>";
        //printf("a = %f \n", input.a);
       }
       else{
          document.getElementById("PEcaseAOutput").innerHTML += "<div>a = 0</div>";
         //printf("a = 0 \n");
       }
          document.getElementById("PEcaseAOutput").innerHTML += "<div>"+input.y+" = a ["+input.b+" ("+input.x+"-"+input.h+") ] + "+input.k+"</div>";
     //printf("%f = a [%f (%f-%f) ] + %f\n", input.y, input.x, input.b, input.h, input.k);;
}
function mainPEcaseA(){
  var res = {
        y: document.getElementById('formPEcaseAy').value,
        x: document.getElementById('formPEcaseAx').value,
        b: document.getElementById('formPEcaseAb').value,
        h: document.getElementById('formPEcaseAh').value,
        k: document.getElementById('formPEcaseAk').value
    };
    res.fpe = (Number(res.x) - Number(res.h)) * Number(res.b);

          if(Number(res.fpe) > 0){
           var intpart = Math.trunc(Number(res.fpe));
           res.a = (Number(res.y) - Number(res.k))/Number(intpart);
          }
          else if(Number(res.fpe) < 0)
          {
           var intpart = (Math.trunc(Number(res.fpe))) - 1;
           res.a = (Number(res.y) - Number(res.k))/Number(intpart);
          }

    printFunctionPEcaseA(res);
}

function printFunctionPEcaseK(input) {

      Number(input.fpe);
      if(Number(input.fpe) > 0){
        document.getElementById("PEcaseKOutput").innerHTML = "<div>k = "+input.k+"</div>";
       //printf("k = %f \n", input.k);
      }
      else if(Number(input.fpe) < 0){
        document.getElementById("PEcaseKOutput").innerHTML += "<div>k = "+input.k+"</div>";
       //printf("k = %f \n", input.k);
      }
      else{
        document.getElementById("PEcaseKOutput").innerHTML += "<div>k = "+input.k+"</div>";
       //printf("k = %f \n", input.y);
      }
        document.getElementById("PEcaseKOutput").innerHTML += "<div>"+input.y+" = "+input.a+" ["+input.b+" ("+input.x+"-"+input.h+") ] + k</div>";

}
function mainPEcaseK(){
  var res = {
        y: document.getElementById('formPEcaseKy').value,
        x: document.getElementById('formPEcaseKx').value,
        a: document.getElementById('formPEcaseKa').value,
        b: document.getElementById('formPEcaseKb').value,
        h: document.getElementById('formPEcaseKh').value
    };
    res.fpe = (Number(res.x) - Number(res.h)) * Number(res.b);
           if(Number(res.fpe) > 0)
           {
            var intpart = Math.trunc(Number(res.fpe));
            res.k = Number(res.y) - (Number(res.a) * Number(intpart));
           }
           else if(Number(res.fpe) < 0)
           {
            var intpart = (Math.trunc(Number(res.fpe))) - 1;
            res.k = Number(res.y) - (Number(res.a) * Number(intpart));
           }

    printFunctionPEcaseK(res);
}

function mainQmissing(choix) {
  switch (choix) {
      case '1' :
          document.getElementById("CanonicCaseY").style.display = 'block';
          document.getElementById("CanonicCaseX").style.display = 'none';
          document.getElementById("CanonicCaseA").style.display = 'none';
          document.getElementById("CanonicCaseK").style.display = 'none';
          document.getElementById("CanonicCaseH").style.display = 'none';
          break;
      case '2' :
          document.getElementById("CanonicCaseY").style.display = 'none';
          document.getElementById("CanonicCaseX").style.display = 'block';
          document.getElementById("CanonicCaseA").style.display = 'none';
          document.getElementById("CanonicCaseK").style.display = 'none';
          document.getElementById("CanonicCaseH").style.display = 'none';
          break;

      case '3' :
          document.getElementById("CanonicCaseY").style.display = 'none';
          document.getElementById("CanonicCaseX").style.display = 'none';
          document.getElementById("CanonicCaseA").style.display = 'block';
          document.getElementById("CanonicCaseK").style.display = 'none';
          document.getElementById("CanonicCaseH").style.display = 'none';
          break;

      case '4' :
          document.getElementById("CanonicCaseY").style.display = 'none';
          document.getElementById("CanonicCaseX").style.display = 'none';
          document.getElementById("CanonicCaseA").style.display = 'none';
          document.getElementById("CanonicCaseH").style.display = 'block';
          document.getElementById("CanonicCaseK").style.display = 'none';
          break;

      case '5' :
          document.getElementById("CanonicCaseY").style.display = 'none';
          document.getElementById("CanonicCaseX").style.display = 'none';
          document.getElementById("CanonicCaseA").style.display = 'none';
          document.getElementById("CanonicCaseH").style.display = 'none';
          document.getElementById("CanonicCaseK").style.display = 'block';
  }
}

function mainQtransform(choix) {
  switch (choix) {
      case '1' :
         document.getElementById("TransformCanonic").style.display = 'block';
         document.getElementById("TransformGenerale").style.display = 'none';
         document.getElementById("TransformFactorisee").style.display = 'none';

          break;
      case '2' :
          document.getElementById("TransformCanonic").style.display = 'none';
          document.getElementById("TransformGenerale").style.display = 'block';
          document.getElementById("TransformFactorisee").style.display = 'none';
          break;

      case '3' :
          document.getElementById("TransformCanonic").style.display = 'none';
          document.getElementById("TransformGenerale").style.display = 'none';
          document.getElementById("TransformFactorisee").style.display = 'block';
  }
}


function mainQanalyse(choix) {
  switch (choix) {
      case '1' :
         document.getElementById("QanalyseCanonique").style.display = 'block';
         document.getElementById("QanalyseGenerale").style.display = 'none';
         document.getElementById("QanalyseFactorisee").style.display = 'none';

          break;
      case '2' :
          document.getElementById("QanalyseCanonique").style.display = 'none';
          document.getElementById("QanalyseGenerale").style.display = 'block';
          document.getElementById("QanalyseFactorisee").style.display = 'none';
          break;

      case '3' :
          document.getElementById("QanalyseCanonique").style.display = 'none';
          document.getElementById("QanalyseGenerale").style.display = 'none';
          document.getElementById("QanalyseFactorisee").style.display = 'block';
  }
}

function mainQ(choixQ) {
  switch (choixQ) {
      case 'A' :
         document.getElementById("Qanalyse").style.display = 'block';
         document.getElementById("Qmissing").style.display = 'none';
         document.getElementById("Qtransform").style.display = 'none';

          break;
      case 'B' :
          document.getElementById("Qanalyse").style.display = 'none';
          document.getElementById("Qmissing").style.display = 'block';
          document.getElementById("Qtransform").style.display = 'none';
          break;
      case 'C' :
          document.getElementById("Qanalyse").style.display = 'none';
          document.getElementById("Qmissing").style.display = 'none';
          document.getElementById("Qtransform").style.display = 'block';
  }
}

function mainPmissing(choix) {
  switch (choix) {
      case '1' :
          document.getElementById("PEcaseY").style.display = 'block';
          document.getElementById("PEcaseA").style.display = 'none';
          document.getElementById("PEcaseK").style.display = 'none';

          break;
      case '2' :
          document.getElementById("PEcaseY").style.display = 'none';
          document.getElementById("PEcaseA").style.display = 'block';
          document.getElementById("PEcaseK").style.display = 'none';

          break;

      case '3' :
          document.getElementById("PEcaseY").style.display = 'none';
          document.getElementById("PEcaseA").style.display = 'none';
          document.getElementById("PEcaseK").style.display = 'block';
          break;
  }
}

function mainP(choixP){
  switch(choixP){
    case 'A' :
      document.getElementById("Pmissing").style.display = 'none';
      document.getElementById("Panalyse").style.display = 'block';
      break;

    case 'B' :
      document.getElementById("Pmissing").style.display = 'block';
      document.getElementById("Panalyse").style.display = 'none';
  }
}

function main(choix) {
  switch(choix) {
    case 'Q' :
      document.getElementById("Qstart").style.display = 'block';
      document.getElementById("Pstart").style.display = 'none';

      document.getElementById("Pmissing").style.display = 'none';
      break;

    case 'P' :
      document.getElementById("Pstart").style.display = 'block';
      document.getElementById("Qstart").style.display = 'none';

      document.getElementById("Qanalyse").style.display = 'none';
      document.getElementById("Qmissing").style.display = 'none';
      document.getElementById("Qtransform").style.display = 'none';
    }
}
